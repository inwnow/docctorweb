import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { adminGuard } from './core/guard/admin.guard';
import { authGuard } from './core/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'doctor',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'users',
    canActivate: [adminGuard],
    loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule),
  },
  {
    path: 'dashboard',
    canActivate: [authGuard],
    loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'settings',
    canActivate: [adminGuard],
    loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'denied',
    loadChildren: () => import('./features/denied/denied.module').then(m => m.DeniedModule)
  },
  {
    path: 'not-found',
    loadChildren: () => import('./features/not-found/not-found.module').then(m => m.NotFoundModule)
  },
  // {
  //   path: 'nurse',
  //   canActivate: [adminGuard],
  //   loadChildren: () => import('./features/nurse/nurse.module').then(m => m.NurseModule)
  // },
  {
    path: 'doctor',
    // canActivate: [adminGuard],
    loadChildren: () => import('./features/doctor/doctor.module').then(m => m.DoctorModule),
    data: {
      breadcrumb: 'หอผู้ป่วย'
    },
  },
  { path: 'test', loadChildren: () => import('./features/test/test.module').then(m => m.TestModule) },
  { path: 'x-ray', loadChildren: () => import('./features/doctor/x-ray/x-ray.module').then(m => m.XRayModule) },
  { path: 'consult', loadChildren: () => import('./features/doctor/consult/consult.module').then(m => m.ConsultModule) },
  { path: 'ekg', loadChildren: () => import('./features/doctor/ekg/ekg.module').then(m => m.EkgModule) },
  { path: 'vital-sign', loadChildren: () => import('./features/doctor/vital-sign/vital-sign.module').then(m => m.VitalSignModule) },
  { path: 'discharge', loadChildren: () => import('./features/doctor/discharge/discharge.module').then(m => m.DischargeModule) },
  { path: 'home-medication', loadChildren: () => import('./features/doctor/home-medication/home-medication.module').then(m => m.HomeMedicationModule) },
  { path: 'refer', loadChildren: () => import('./features/doctor/refer/refer.module').then(m => m.ReferModule) },
  { path: 'cause-of-death', loadChildren: () => import('./features/doctor/cause-of-death/cause-of-death.module').then(m => m.CauseOfDeathModule) },
  { path: 'follow-up', loadChildren: () => import('./features/doctor/follow-up/follow-up.module').then(m => m.FollowUpModule) },
  { path: 'nurse-note', loadChildren: () => import('./features/doctor/nurse-note/nurse-note.module').then(m => m.NurseNoteModule) },
  { path: 'doctor-car', loadChildren: () => import('./features/doctor/doctor-care/doctor-care.module').then(m => m.DoctorCareModule) },
  { path: 'doctor-care', loadChildren: () => import('./features/doctor/doctor-care/doctor-care.module').then(m => m.DoctorCareModule) },

  { path: '**', redirectTo: 'not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
