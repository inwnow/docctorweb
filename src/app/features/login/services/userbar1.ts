import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-bar1',
  template: `me
  <!-- <div nz-col nzSpan="12" class="flex justify-end items-center">
            <nz-button-group class="border-0">
                <button nz-button nz-dropdown [nzDropdownMenu]="menu1" nzPlacement="bottomRight"
                    class="rounded-2xl flex justify-end items-center bg-inherit">
                    สวัสดีคุณ :{{user_login_name}}&nbsp;<span nz-icon nzType="user"
                        class="text-green-500 text-md"></span>
                </button>
            </nz-button-group>
            <nz-dropdown-menu #menu1="nzDropdownMenu">
                <ul nz-menu>
                    <li nz-menu-item><i class="fas fa-user-edit text-gray-400 text-md"></i>&nbsp;User Profies</li>
                    <li nz-menu-item><i class="fas fa-user-edit text-gray-400 text-md"></i>&nbsp;Avartar</li>
                    <li nz-menu-item (click)="logOut()"><i class="fas fa-power-off text-gray-400 text-md"></i>&nbsp;Log
                        Out</li>
                </ul>
            </nz-dropdown-menu>
        </div> -->
        `,
})
export class UserbarComponent  {
  user_login_name: any;
 

  constructor(  private router: Router,) {
    this.user_login_name  =  sessionStorage.getItem('userLoginName'); 
  }


  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }


}
