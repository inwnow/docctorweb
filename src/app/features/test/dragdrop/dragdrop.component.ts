import { Component } from '@angular/core';
import { CdkDrag, CdkDragDrop, CdkDragEnd, CdkDragStart, CdkDropListGroup, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-dragdrop',
  templateUrl: './dragdrop.component.html',
  styleUrls: ['./dragdrop.component.css'],

})
export class DragdropComponent {
  private initialX = 0;
  myEvent: string = 'middle';

  onDragStarted(event: CdkDragStart) {
    this.initialX = event.source._dragRef.getFreeDragPosition().x;
  }

  // onDragEnded(event: CdkDragEnd) {
  //   // Handle drag end event here
  //   console.log('Drag ended', event);
  //   alert('drag drop okd');
  // }
  onDragEnded(event: CdkDragEnd) {
    const finalX = event.source._dragRef.getFreeDragPosition().x; 
    if (finalX > this.initialX) {
      console.log('Dragged to the right');
      const dk = 'right';
      if (dk == 'right' && this.myEvent == 'left') {
        this.myEvent = 'right-left';
        console.log(this.myEvent);
      } else if (dk == 'right' && this.myEvent == 'right') {
        this.myEvent = 'right-right';
        console.log(this.myEvent);
      } else {
        this.myEvent = 'right-middle';
        console.log(this.myEvent);
      }

    } else if (finalX < this.initialX) {
      console.log('Dragged to the left');
      const dk = 'left';
      if (dk == 'left' && this.myEvent == 'right') {
        this.myEvent = 'left-right';
        console.log(this.myEvent);
      } else if (dk == 'left' && this.myEvent == 'left') {
        this.myEvent = 'left-left';
        console.log(this.myEvent);
      } else {
        this.myEvent = 'left-middle';
        console.log(this.myEvent);
      }

    } 
    
  }
  openPanel(side:string){
    this.myEvent = side;
  }

}
