import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DischargeComponent } from './discharge.component';

const routes: Routes = [{ path: '', component: DischargeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DischargeRoutingModule { }
