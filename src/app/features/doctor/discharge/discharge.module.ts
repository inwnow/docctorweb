import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DischargeRoutingModule } from './discharge-routing.module';
import { DischargeComponent } from './discharge.component';


@NgModule({
  declarations: [
    DischargeComponent
  ],
  imports: [
    CommonModule,
    DischargeRoutingModule
  ]
})
export class DischargeModule { }
