import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CauseOfDeathComponent } from './cause-of-death.component';

const routes: Routes = [{ path: '', component: CauseOfDeathComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CauseOfDeathRoutingModule { }
