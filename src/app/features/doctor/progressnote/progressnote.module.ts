import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgressnoteRoutingModule } from './progressnote-routing.module';
import { ProgressnoteComponent } from './progressnote.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonModule } from 'ng-zorro-antd/button';

@NgModule({
  declarations: [
    ProgressnoteComponent
  ],
  imports: [

    NgZorroModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    ProgressnoteRoutingModule,
    NzCheckboxModule,
    NzButtonModule
  ]
})
export class ProgressnoteModule { }
