import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressnoteComponent } from './progressnote.component';

describe('ProgressnoteComponent', () => {
  let component: ProgressnoteComponent;
  let fixture: ComponentFixture<ProgressnoteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressnoteComponent]
    });
    fixture = TestBed.createComponent(ProgressnoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
