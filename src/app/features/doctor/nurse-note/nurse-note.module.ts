import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NurseNoteRoutingModule } from './nurse-note-routing.module';
import { NurseNoteComponent } from './nurse-note.component';


@NgModule({
  declarations: [
    NurseNoteComponent
  ],
  imports: [
    CommonModule,
    NurseNoteRoutingModule
  ]
})
export class NurseNoteModule { }
