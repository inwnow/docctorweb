import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeMedicationComponent } from './home-medication.component';

const routes: Routes = [{ path: '', component: HomeMedicationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeMedicationRoutingModule { }
