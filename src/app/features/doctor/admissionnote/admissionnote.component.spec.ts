import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionnoteComponent } from './admissionnote.component';

describe('AdmissionnoteComponent', () => {
  let component: AdmissionnoteComponent;
  let fixture: ComponentFixture<AdmissionnoteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmissionnoteComponent]
    });
    fixture = TestBed.createComponent(AdmissionnoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
