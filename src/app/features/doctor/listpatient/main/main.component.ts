import { Component, ElementRef, ViewChild, Renderer2,HostListener, EventEmitter, Output} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NotificationService } from '../../../../shared/services/notification.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { UserProfileService } from '../../../../core/services/user-profiles.service';
import { NgxSpinnerService } from "ngx-spinner";
import {  CdkDragEnd, CdkDragStart } from '@angular/cdk/drag-drop';
import {DeviceCheckComponent} from '../../listward/app-device-check';
import {VariableShareService} from '../../../../core/services/variable-share.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {

  query: any = '';
  dataSet: any[] = [];
  loading = false;
  patientList: any;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  myEvent: string = 'middle';
  queryParamsData: any;
  item: any = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  @ViewChild('target', { static: false }) target!: ElementRef;
  @ViewChild('exemplo', { read: ElementRef }) exemplo!: ElementRef;
  private initialX = 0; 
  items:any = [];
  deviceType: string = 'Unknown';
  @Output() triggerParentMethod = new EventEmitter<void>();


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private doctorService: DoctorService,
    private nzMessageService: NzMessageService,
    private notificationService: NotificationService,
    private modal: NzModalService,
    private userProfileService: UserProfileService,
    private spinner: NgxSpinnerService,
    private renderer: Renderer2,
    private breakpointObserver: BreakpointObserver,
    private variableShareService:VariableShareService


  ) {
    this.user_login_name = this.userProfileService.fname;
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    const localStorageWardId = localStorage.getItem('ward_id');
    if(jsonObject != null || jsonObject != undefined){
        this.queryParamsData = jsonObject;
    }else{
      this.queryParamsData = localStorageWardId;
    } 

    console.log(this.queryParamsData);

  }
  //  - /doctor/ward/:ward_id/patient-admit   #GET

  ngOnInit(): void {
    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.closeSidebar();
    this.user_login_name = sessionStorage.getItem('userLoginName');
    this.checkDevice();
  
    this.getPatientAdmit();

  }
  /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
  addItem(newItem: string) {
    this.items.push(newItem);
    console.log(this.item);
  }

  closeSidebar(): void {
    this.variableShareService.setCloseSidebar(true);
  }
  doSearch() {
    this.getPatientAdmit();
  }
  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getPatientAdmit();
  }

  checkDevice(){
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[Breakpoints.XSmall]) {
          this.deviceType = 'xs';
         
        } else if (result.breakpoints[Breakpoints.Small]) {
          this.deviceType = 'sm';
        
        } else if (result.breakpoints[Breakpoints.Medium]) {
          this.deviceType = 'md';
        
        } else if (result.breakpoints[Breakpoints.Large]) {
          this.deviceType = 'lg';
        
        }else{
            this.deviceType = 'xl';
         
        }
      }
    });
  }
  @HostListener('window:resize', ['$event'])

  onDragStarted(event: CdkDragStart,i:any) {
    this.initialX = event.source._dragRef.getFreeDragPosition().x;
  }

  onDragEnded(event: CdkDragEnd,i:any) {
    const finalX = event.source._dragRef.getFreeDragPosition().x; 
    if (finalX > this.initialX) {
      console.log('Dragged to the right');
      this.myEvent = 'right';
      this.patientList[i].hideLeft = false;   
      this.patientList[i].hideRight = true;
     

    } else if (finalX < this.initialX) {
      console.log('Dragged to the left');
      this.myEvent = 'left';
      this.patientList[i].hideRight = false;   
    this.patientList[i].hideLeft = true;

    } 
    
  }
  openPanelLeft( i: any) {
    this.patientList[i].hideLeft = false;   
    this.patientList[i].hideRight = true;

  }
  openPanelLeft2(i: any) {
    this.patientList[i].hideLeft = true;
    this.patientList[i].hideRight = true;
  }
  openPanelRight( i: any) {
    this.patientList[i].hideRight = false;   
    this.patientList[i].hideLeft = true;
  }
  openPanelRight2(i: any) {
    this.patientList[i].hideRight = true;
    this.patientList[i].hideLeft = true;
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


  cancel(): void {
    this.nzMessageService.info('click cancel');
  }

  confirm(): void {
    this.nzMessageService.info('click confirm');
  }

  beforeConfirm(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(true);
      }, 3000);
    });
  }

  //////////////////////




  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getPatientAdmit()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getPatientAdmit()
  }

  async getPatientAdmit() {
    this.spinner.show();
    try {
      const response = await this.doctorService.getPatientAdmit(this.queryParamsData);
      const data = await response.data;
      // console.log(data);
      this.patientList = data.data;
      this.patientList.forEach((patient: { hideLeft: boolean;hideRight: boolean; }) => {
        // Add the 'hide' field to each object and set its value to true
        patient.hideLeft = true;
        patient.hideRight = true;
      });
      console.log(this.patientList);

      this.hideSpinner();
    } catch (error: any) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  applyStyles() {
    this.exemplo.nativeElement.classList.add("text-green-600");
    // const targetElement = document.querySelector('.target') as HTMLElement;
    // if (targetElement) {
    //   targetElement.style.color = 'red';
    //   console.log(targetElement);

    // }else{
    //   console.log('no el0000');
    // }
    // if (this.targetElement) {
    //   // Access the target element using the ViewChild property
    //   const target: HTMLElement = this.targetElement.nativeElement;
    //   console.log(target);
    //   // Apply styles using the renderer
    //   this.renderer.setStyle(target, 'color', 'red');
    // }else{
    //   console.log('no el');
    // }
  }
  navigatePatientInfo(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.closeSidebar();
    this.router.navigate(['/doctor/list-patient/patient-info'], { queryParams: { data: jsonString } });

  }
  navigateDoctorOrder(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.closeSidebar();
    // this.router.navigate(['/doctor/doctor-order'], { queryParams: { data: jsonString } });
    this.router.navigate(['/doctor/list-patient/doctor-order'], { queryParams: { data: jsonString } });

  }
  navigateAdmisstionNote(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.closeSidebar();
    this.router.navigate(['/doctor/list-patient/admission-note'], { queryParams: { data: jsonString } });

  }
  navigateProgressNote(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.closeSidebar();
    this.router.navigate(['/doctor/list-patient/progress-note'], { queryParams: { data: jsonString } });

  }
}
